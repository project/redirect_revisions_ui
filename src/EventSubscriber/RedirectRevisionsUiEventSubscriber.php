<?php

namespace Drupal\redirect_revisions_ui\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Media Revisions UI event subscriber.
 */
class RedirectRevisionsUiEventSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $revisionRoute = $collection->get('entity.redirect.revision');
    if (!is_null($revisionRoute)) {
      $revisionRoute->setOption('_admin_route', TRUE);
    }

    $versionHistoryRoute = $collection->get('entity.redirect.version_history');
    if (!is_null($versionHistoryRoute)) {
      $versionHistoryRoute->setOption('_admin_route', TRUE);
    }

    $revisionRevertFormRoute = $collection->get('entity.redirect.revision_revert_form');
    if (!is_null($revisionRevertFormRoute)) {
      $revisionRevertFormRoute->setOption('_admin_route', TRUE);
    }

    $revisionDeleteFormRoute = $collection->get('entity.redirect.revision_delete_form');
    if (!is_null($revisionDeleteFormRoute)) {
      $revisionDeleteFormRoute->setOption('_admin_route', TRUE);
    }
  }

}

